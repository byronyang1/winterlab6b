public class GameManager {
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	public GameManager() {
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
	}
	public String toString() {
		return "Center card: " + this.centerCard + " \nPlayer card: " + this.playerCard;
	}
	public void dealCards() {
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
		this.drawPile.shuffle();
	}
	public int getNumberOfCards() {
		return this.drawPile.length();
	}
	public int calculatePoints() {
		if (this.centerCard.getValue() == this.playerCard.getValue()) {
			return 4;
		} else if (this.centerCard.getSuit() == this.playerCard.getSuit()) {
			return 2;
		} else {
			return -1;
		}
	}
}
	
	