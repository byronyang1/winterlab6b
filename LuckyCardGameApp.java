import java.util.Scanner;
public class LuckyCardGameApp {
	public static void main(String[] args) {
		GameManager manager = new GameManager();
		int totalPoints = 0;
		System.out.println("Welcome to Lucky! Card game!");
		int i = 1;
		while (manager.getNumberOfCards() > 1 && totalPoints < 5) {
			System.out.println("Round " + i);
			System.out.println(manager);
			totalPoints += manager.calculatePoints();
			System.out.println(totalPoints);
			System.out.println("Your points after round " + i + " : " + totalPoints);
			manager.dealCards();
			i++;
		}
		System.out.println("The final score is: " + totalPoints);
		if (totalPoints < 5) {
			System.out.println("The player lost the game");
		} else {
			System.out.println("The player has won the game");
		}
	}
}